# LAB2 - NLP - Text Classification

## Author

| Members       | ID       |
| ------------- | -------- |
| Nguyễn Kha Vĩ | 19120715 |

## Env

| OS       | Ubuntu 20.04 LTS |
| -------- | ---------------- |
| Language | Python3          |
| Editor   | VSCode           |

## Command:

-   Install necessary libraries: `pip3 install -r requirements.txt`
-   Run: `python3 text_classification.py`

## Sample result

| Key                                          | Value              |
| -------------------------------------------- | ------------------ |
| Number of classes                            | 6                  |
| Vocabulary size (English stop words removed) | 8662               |
| Number of training samples                   | 5452               |
| Number of testing samples                    | 500                |
| Top words size (from NLTK library)           | 179                |
| Precision                                    | 78.60000000000001% |
| Recall                                       | 86.15768697198453% |
| F-score                                      | 82.20550215844555  |
