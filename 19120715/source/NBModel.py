import numpy as np
from collections import defaultdict
from Helper import Helper


class NBModel:
    def __init__(self) -> None:
        self.trained = False
        self.tested = False

    def addToBoW(self, sentence, class_index):
        for word in Helper.preprocess_sentence(sentence, self.stop_words, self.lang).split():
            # count the number of word in the sentence
            self.bow_dicts[class_index][word] += 1

    def train(self, data, labels, lang='vn'):
        '''
        for each word w [ count(w|c)+1 ] / [ count(c) + |V| + 1 ] } * p(c)
        '''
        print('Training...')
        self.lang = lang
        self.stop_words = Helper.read_stop_words(lang)
        self.classes = np.unique(labels)
        self.num_classes = self.classes.shape[0]

        self.data = data
        self.labels = labels
        self.bow_dicts = np.array([defaultdict(int)
                                  for _ in range(self.num_classes)])

        # Calculate p(c) for each class (priors[class])
        priors = {}
        num_samples = len(self.labels)

        for class_index in self.classes:
            # Filter data where label is equal to class (class_index)
            sentences = []
            for i in range(0, len(self.data)):
                if self.labels[i] == class_index:
                    sentences.append(self.data[i])
            # Calculate p(c)
            priors[class_index] = len(sentences)/num_samples

            # for each sentence of the class (class_index), add it to bag of words
            for sentence in sentences:
                self.addToBoW(sentence, class_index)

        all_words = []
        class_word_counts = np.empty(self.num_classes)
        for class_index in self.classes:

            class_word_counts[class_index] = np.sum(
                np.array(list(self.bow_dicts[class_index].values())))+1

            # get all words of this category
            all_words += self.bow_dicts[class_index].keys()

        # combine all words of every category & make them unique to get vocabulary -V- of entire training set

        # This is V (vocabulary)
        self.vocab = np.unique(np.array(all_words))
        # |V|
        self.vocab_length = self.vocab.shape[0]

        # computing denominator value
        denoms = np.array([class_word_counts[class_index] +
                          self.vocab_length + 1 for class_index in self.classes])

        classes_info = [(self.bow_dicts[class_index], priors[class_index],
                         denoms[class_index]) for class_index in self.classes]
        self.classes_info = np.array(classes_info)
        print('Training finished!')
        self.trained = True

    def getSentenceProb(self, test_sentence):
        likelihood_prob = np.zeros(self.classes.shape[0])
        for class_index in self.classes:
            for test_token in Helper.preprocess_sentence(test_sentence, self.stop_words, self.lang).split():
                '''
                for each word w [ count(w|c)+1 ] / [ count(c) + |V| + 1 ]
                '''
                class_info = self.classes_info[class_index]
                # count(w|c)+1
                test_token_counts = class_info[0].get(test_token, 0)+1

                # [ count(w|c)+1 ] / [ count(c) + |V| + 1 ]
                test_token_prob = test_token_counts / float(class_info[2])

                # get log to avoid underflow (test_token_prob may be extremely tiny)
                likelihood_prob[class_index] += np.log(test_token_prob)

        prob = np.empty(self.num_classes)
        for class_index in self.classes:
            prob[class_index] = likelihood_prob[class_index] + \
                np.log(self.classes_info[class_index][1])
        return prob

    def test(self, test_data, test_labels):
        self.test_labels = test_labels
        if not self.trained:
            raise Exception('Model not trained!')
        print('Testing...')
        predictions = []  # to store prediction of each test sentence
        for test_sentence in test_data:
            predictions.append(self.predict(test_sentence))
        predicted = np.array(predictions)
        self.precision = np.mean(predicted == test_labels)*100

        # calculate recall for each class
        self.recalls = {}
        for class_index in self.classes:
            # Filter data where label is equal to class (class_index)
            sentences = []
            for i in range(0, len(test_data)):
                if test_labels[i] == class_index:
                    sentences.append(test_data[i])

            # Filter data where label is equal to class (class_index)
            predicted_sentences = []
            for i in range(0, len(predicted)):
                if predicted[i] == class_index:
                    predicted_sentences.append(predicted[i])

            # Calculate p(c)
            self.recalls[class_index] = len(predicted_sentences)/len(sentences)

        self.recall = np.mean(list(self.recalls.values()))*100
        # calculate F
        self.f = 2*self.precision*self.recall/(self.precision+self.recall)
        print('Testing finished!')
        self.tested = True

    def predict(self, sentence):
        prob = self.getSentenceProb(sentence)
        # map to classes
        return self.classes[np.argmax(prob)]

    def summary(self):
        if not self.trained:
            raise Exception('Model not trained!')
        print('Summary:')
        print(f' > Number of classes: {self.num_classes}')
        print(f' > Vocabulary size: {self.vocab_length}')
        print(f' > Number of training samples:', len(self.labels))
        print(f' > Number of testing samples:', len(self.test_labels))
        print(f' > Top words size: {len(self.stop_words)}')
        print(f' > Precision: {self.precision}%')
        print(f' > Recall: {self.recall}%')
        for class_index in self.classes:
            print(
                f'  - Recall for class {class_index}: {self.recalls[class_index]*100}%')
        print(f' > F-score: {self.f}')
