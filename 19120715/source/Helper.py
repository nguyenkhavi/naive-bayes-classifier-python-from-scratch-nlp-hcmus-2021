import re
from underthesea import word_tokenize
from argparse import ArgumentParser
import string


class Helper:
    @staticmethod
    def read_stop_words(lang='vi'):
        print(f'''Reading stop words from {lang}''')
        if lang == 'vi':
            with open('vn_stop_words.txt') as f:
                stopwords = []
                for line in f:
                    stopwords.append("_".join(line.strip().split()))
            return set(stopwords)
        elif lang == 'en':

            from nltk.corpus import stopwords
            return stopwords.words('english')
        return set()

    @staticmethod
    def load_train_data():
        train_data = []
        with open("TREC.train.all", "rb") as fin:
            for line in fin.readlines():
                text = line.strip().decode("utf-8", "ignore")
                train_data.append(text)

        docs = [line[2:] for line in train_data]
        labels = [int(line[0:2]) for line in train_data]

        return docs, labels

    @staticmethod
    def load_test_data():
        test_data = []
        with open("TREC.test.all", "rb") as fin:
            for line in fin.readlines():
                text = line.strip().decode("utf-8", "ignore")
                test_data.append(text)

        docs = [line[2:] for line in test_data]
        labels = [int(line[0:2]) for line in test_data]
        return docs, labels

    # static method remove stopwords by lang = 'vn' or 'en'

    @staticmethod
    def preprocess_sentence(sentence, stop_words, lang='vi'):
        cleaned_sent = sentence.lower()
        if lang == 'en':
            from nltk import tokenize, download

            try:
                cleaned_sent = " ".join(tokenize.word_tokenize(cleaned_sent))
            except:
                download('punkt')
                cleaned_sent = " ".join(tokenize.word_tokenize(cleaned_sent))

        elif lang == 'vi':
            # remove email
            cleaned_sent = re.sub('\S*@\S*\s?', '', sentence)
            # remove punctuation
            cleaned_sent = cleaned_sent.translate(
                str.maketrans({key: None for key in string.punctuation}))
            # remove numeric
            cleaned_sent = cleaned_sent.translate(
                str.maketrans({key: None for key in string.digits}))
            # word tokenize
            cleaned_sent = word_tokenize(cleaned_sent, format='text')

            # eliminate stop words and to lower case
            cleaned_sent = " ".join([word.replace(' ', '_').lower().strip()
                                    for word in cleaned_sent if word not in stop_words])

        return cleaned_sent

    @staticmethod
    def get_input_data():
        parser = ArgumentParser()
        parser.add_argument('--input')
        parser.add_argument('--result')

        args = parser.parse_args()
        return args.input, args.result

    @staticmethod
    def write_result(result, file_name):
        with open(file_name, "w") as fout:
            fout.write(result)
