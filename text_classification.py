from NBModel import NBModel
from Helper import Helper

if __name__ == '__main__':
    train_data, train_labels = Helper.load_train_data()

    test_data, test_labels = Helper.load_test_data()

    model = NBModel()
    model.train(train_data, train_labels, 'en')
    model.test(test_data, test_labels)
    model.summary()
    print('Runned successfully!')
